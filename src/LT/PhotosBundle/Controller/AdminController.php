<?php

namespace LT\PhotosBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AdminController extends Controller {
    public function indexAction() {
	return $this->render('LTPhotosBundle:Admin:index.html.twig');
    }
}
