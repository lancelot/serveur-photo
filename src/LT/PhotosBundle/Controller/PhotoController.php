<?php

namespace LT\PhotosBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use LT\PhotosBundle\Entity\Photo;
use LT\PhotosBundle\Entity\Event;
use LT\PhotosBundle\Form\PhotoType;
use LT\PhotosBundle\Form\EventType;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Domain\UserSecurityIdentity;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;

class PhotoController extends Controller {
    public function uploadAction(Request $request) {
        $photo = new Photo();

        $photograph = $this->getUser()->getPhotograph();

        $photo->setPhotograph($photograph);

        $form = $this->get('form.factory')->create(new PhotoType, $photo);

        //création d'un évènement
        $new_event = new Event();
        $form_event = $this->createForm(new EventType(), $new_event, array(
                    'action' => $this->generateUrl('event_create'),
                    'method' => 'POST',
        ));

        $form_event->add('submit', 'submit', array('label' => 'Créer'));
        //formulaire de création d'un nouvel event

        if ($form->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            if (!in_array($photograph, $photo->getEvent()->getPhotographs()->toArray()))
                $photo->getEvent()->addPhotograph($photo->getPhotograph());

            $em->persist($photo);
            $em->flush();
            //rediriger vers une page
        }

        $events = $this->getDoctrine()->getManager()->getRepository('LTPhotosBundle:Event')->findAll();

        return $this->render('LTPhotosBundle:Default:upload.html.twig', array('form' => $form->createView(), 'form_event' => $form_event->createView(), 'events' => $events));
    }

    public function uploadPhotosAction(Request $request, $slug) {
        $repository = $this->getDoctrine()->getManager()->getRepository('LTPhotosBundle:Event');
        $event = $repository->findOneBy(array('slug' => $slug));
        $photo = new Photo();

        $form = $this->createFormBuilder(array())
                        ->add('files', 'file', array('attr' => array('multiple' => true)))
                        ->add('save', 'submit')
                        ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();
            $photograph = $this->getUser()->getPhotograph();
            $em = $this->getDoctrine()->getManager();
	    $photos = array();

            foreach($data['files'] as $file) {
                $photos[] = new Photo();
		$photo = end($photos);
                $photo->setFile($file);
                $photo->setPhotograph($photograph);
                $photo->setValid(false);

                $event->addPhoto($photo);
                if (!in_array($photograph, $photo->getEvent()->getPhotographs()->toArray()))
                    $photo->getEvent()->addPhotograph($photo->getPhotograph());
                $em->persist($event);
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($event);
	    $em->flush();

	    foreach($photos as $photo) {
		$aclProvider = $this->get('security.acl.provider');
                $objectIdentity = ObjectIdentity::fromDomainObject($photo);

                $acl = $aclProvider->createAcl($objectIdentity);

                $securityContext = $this->get('security.context');
                $user = $securityContext->getToken()->getUser();
                $securityIdentity = UserSecurityIdentity::fromAccount($user);

                $acl->insertObjectAce($securityIdentity, MaskBuilder::MASK_OWNER);
                $aclProvider->updateAcl($acl);
	    }


        }
        $form=$form->createView();
        $form->children['files']->vars['full_name'] = 'form[files][]';

        return $this->render('LTPhotosBundle:Default:uploadPhotos.html.twig', array('form' => $form));
    }

}
