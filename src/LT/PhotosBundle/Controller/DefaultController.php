<?php

namespace LT\PhotosBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use LT\PhotosBundle\Entity\FormContact;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('LTPhotosBundle:Default:index.html.twig');
    }

    public function recentAction() {
	$repository = $this
	  ->getDoctrine()
	  ->getManager()
	  ->getRepository('LTPhotosBundle:Event')
	;
	$listEvents=array();
	$listAllEvents = $repository->findBy(
	  array(), // Critere
	  array('date' => 'desc'),        // Tri
	  10,                              // Limite
	  0                               // Offset
	);

	foreach ($listAllEvents as $event) {
	  if (count($event->getPhotos()) != 0)
	    $listEvents[] = $event;
	}

	return $this->render('LTPhotosBundle:Default:recent.html.twig', array('listEvents' => $listEvents));
    }

    public function yearAction($year1, $year2) {
	$repository = $this
	  ->getDoctrine()
	  ->getManager()
	  ->getRepository('LTPhotosBundle:Event')
	;

	$listEvents = $repository->findByScolarYear($year1, $year2);

	return $this->render('LTPhotosBundle:Default:year.html.twig', array('listEvents' => $listEvents));
    }

    public function eventAction() {
	$repository = $this->getDoctrine()->getManager()->getRepository('LTPhotosBundle:Event');

	$listAllEvents = $repository->findThisYear();
	$listEvents=array();

	foreach ($listAllEvents as $event) {
          if (count($event->getPhotos()) != 0)
            $listEvents[] = $event;
        }

	return $this->render('LTPhotosBundle:Default:recent.html.twig', array('listEvents' => $listEvents));
    }

    public function photographAction($year1, $year2, $slug) {

	return $this->redirect($this->generateUrl('lt_photos_gallery', 
							array('year1' => $year1, 
							      'year2' => $year2, 
							      'slug' => $slug, 
							      'photograph' => null)
						 )
			      );
    }

    public function archivesAction() {
	$repository = $this->getDoctrine()->getManager()->getRepository('LTPhotosBundle:Event');
	$firstEvent = $repository->findBy(
	  array(),
	  array('date' => 'asc'),
	  1,
	  0
	);

	$lastEvent = $repository->findBy(
	  array(),
	  array('date' => 'desc'),
	  1,
	  0
	);

	$firstYear = $firstEvent[0]->getDate()->format('Y') + floor($firstEvent[0]->getDate()->format('m') / 9) - 1;
	$lastYear = $lastEvent[0]->getDate()->format('Y') + floor($lastEvent[0]->getDate()->format('m') /9) - 1;

	$j=0;
	for ($i = $firstYear ; $i <= $lastYear ; $i++) {
	    $years[$j] = strval($i)."-".strval($i+1);
	    $j++;
	}

	return $this->render('LTPhotosBundle:Default:archives.html.twig', array('years' => $years));
    }

    public function galleryAction($year1, $year2, $slug, $photograph) {
	$repository = $this->getDoctrine()->getManager()->getRepository('LTPhotosBundle:Event');
	$event = $repository->findOneBy(array('slug' => $slug));

	$repository = $this->getDoctrine()->getManager()->getRepository('LTPhotosBundle:Photo');
	$photos = $repository->findBy(
	    array('event' => $event, 'valid' => true),
	    array(),
	    null,
	    null);

	return $this->render('LTPhotosBundle:Default:gallery.html.twig', array('photos' => $photos, 'photographs' => $event->getPhotographs()));
    }
}

