<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new LT\PhotosBundle\LTPhotosBundle(),
            new LT\UserBundle\LTUserBundle(),
	    new Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle(),
	    new Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle(),
	    new FOS\UserBundle\FOSUserBundle(),
	    new Sonata\CoreBundle\SonataCoreBundle(),
	    new Sonata\BlockBundle\SonataBlockBundle(),
	    new Knp\Bundle\MenuBundle\KnpMenuBundle(),
	    new Sonata\DoctrineORMAdminBundle\SonataDoctrineORMAdminBundle(),
	    new Sonata\AdminBundle\SonataAdminBundle(),
	    new Genemu\Bundle\FormBundle\GenemuFormBundle(),
	    new PunkAve\FileUploaderBundle\PunkAveFileUploaderBundle(),
	    new Liip\ImagineBundle\LiipImagineBundle(),
	    new RMS\PushNotificationsBundle\RMSPushNotificationsBundle(),
	    //new Genemu\Bundle\FormBundle\GenemuFormBundle(),
	    new Oneup\UploaderBundle\OneupUploaderBundle(),
            new LT\CoreBundle\LTCoreBundle(),
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'))) {
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
	    $bundles[] = new Elao\WebProfilerExtraBundle\WebProfilerExtraBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__.'/config/config_'.$this->getEnvironment().'.yml');
    }
}
